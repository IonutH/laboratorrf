package ro.usv.rf;

public class MainClass {
	
	
	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patters and %s features", numberOfPatterns, numberOfFeatures));
		
			for(int i=1; i<learningSet.length;i++) {
				System.out.println("Euclidian Distance between 0 and "+ i + " is: " + DistanceUtils.euclidianDistance(learningSet[0], learningSet[i]));
				System.out.println("Mahalanobis between 0 and "+ i + " is: " + DistanceUtils.mahalanobisDistance(learningSet[0], learningSet[i], learningSet.length));
				System.out.println("Cebisev Distance between 0 and "+ i + " is: " + DistanceUtils.cebisevDistance(learningSet[0], learningSet[i]));
				System.out.println("City Block Distance between 0 and "+ i + " is: " + DistanceUtils.cityBlockDistance(learningSet[0], learningSet[i]));
				System.out.println();
			}
			
			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
		
	}
	}
