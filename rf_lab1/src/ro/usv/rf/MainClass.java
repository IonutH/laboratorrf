package ro.usv.rf;

public class MainClass {

	public static void main(String[] args) {
		double[][] learningSet = FileUtils.readLearningSetFromFile("in.txt");
		FileUtils.writeLearningSetToFile("out.csv", normalizeLearningSet(learningSet));

	}

	private static double[][] normalizeLearningSet(double[][] learningSet) {
		double[][] normalizedLearningSet = new double[learningSet.length][learningSet[0].length];
		double[] max = new double[learningSet[0].length];
		double[] min = new double[learningSet[0].length];
		
		int i,j;
		
		for(j=0; j< learningSet[0].length; j++) {
			min[j] = max[j] = learningSet[0][j];
			for(i = 0; i < learningSet.length; i++) {
				min[j] = Double.min(min[j], learningSet[i][j]);
				max[j] = Double.max(max[j], learningSet[i][j]);
			}
		}
		
		for(j=0; j < learningSet[0].length; j++) {
			for( i=0; i < learningSet.length; i++) {
				normalizedLearningSet[i][j] = (learningSet[i][j]-min[j]) / (max[j] - min[j]);
			}
		}
		
		
		
		return normalizedLearningSet;
	}
}
