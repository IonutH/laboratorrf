package ro.usv.rf;

public class DistanceUtils {
	protected static double euclidianDistance(double[] firstCharacteristic, double[] secondCharacteristic) {
		return Math.sqrt( Math.pow(firstCharacteristic[0] - secondCharacteristic[0], 2) + Math.pow(firstCharacteristic[1] - secondCharacteristic[1], 2));
	}
	
	protected static double euclidianDistanceGeneralized(double[] firstCharacteristic, double[] secondCharacteristic) {
		double sum =0;
		for(int i=0; i<firstCharacteristic.length-1;i++) {
			sum += Math.pow(firstCharacteristic[i] - secondCharacteristic[i], 2);
		}
		return Math.floor(Math.sqrt(sum) * 100.0) /100.0;
	}
	
}
