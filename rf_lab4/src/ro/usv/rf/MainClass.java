package ro.usv.rf;

import java.util.Arrays;

public class MainClass {
	
	
	public static void main(String[] args) {
		String[][] stringLearningSet;
		double[][] learningSet;
		double[][] distanceMatrix;
		String[] classes;
		try {
			stringLearningSet = FileUtils.readLearningSetFromFile("in.txt");
			learningSet = getDoubleLearningSet(stringLearningSet);
			classes = getClassesArray(stringLearningSet);
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patters and %s features", numberOfPatterns, numberOfFeatures));
			distanceMatrix = new double[numberOfPatterns][];
			for(int i=0; i< learningSet.length;i++) {
				distanceMatrix[i] = new double[numberOfPatterns];				
			}
			
			for(int i=0; i< learningSet.length;i++) {
				for(int j=i+1; j< learningSet.length;j++) {
					distanceMatrix[i][j] = distanceMatrix[j][i]= DistanceUtils.euclidianDistanceGeneralized(learningSet[i], learningSet[j]);
				}
			}
			
			FileUtils.writeLearningSetToFile("distanceMatrix.out", distanceMatrix);
			
			for(int i=0; i<stringLearningSet.length;i++) {
				if(stringLearningSet[i].length < FileUtils.LENGTH_WITH_CLASS) {
					int pos = 0;
					double minValue = distanceMatrix[i][0];
					for(int j=1; j<distanceMatrix[i].length;j++) {
						if(distanceMatrix[i][j]!=0) {
							if(distanceMatrix[i][j] < minValue) {
								minValue = distanceMatrix[i][j];
								pos = j;								
							}
						}
					}

					System.out.println("Nearest Neighbour distance: " + minValue);
					System.out.println(i + "th Form is in " + pos + "'s form class -> " + classes[pos]);
				}
			}
			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
		
	}
	
	private static double[][] getDoubleLearningSet(String[][] learningSet){
		double[][] doubleLearningSet = new double[learningSet.length][];
		for(int i=0; i< learningSet.length;i++) {
			doubleLearningSet[i] = new double[learningSet[i].length];
			for(int j=0; j<learningSet[i].length; j++) {
				try {
					doubleLearningSet[i][j] = Double.valueOf(learningSet[i][j]);
					System.out.print(doubleLearningSet[i][j] + " ");
					
				} catch(NumberFormatException| ArrayIndexOutOfBoundsException e ) {
					
				}
			}
			System.out.println();
		}
		
		return doubleLearningSet;
	}
	
	private static String[] getClassesArray(String[][] learningSet) {
		String[] array = new String[learningSet.length];
		for(int i=0; i<learningSet.length;i++) {
			array[i] =learningSet[i][learningSet[i].length -1]; 
		}
		return array;
	}
}
