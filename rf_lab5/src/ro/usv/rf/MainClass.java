package ro.usv.rf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class MainClass {
	
	
	public static void main(String[] args) {
		String[][] stringLearningSet;
		double[][] learningSet;
		double[][] distanceMatrix;
		String[] classes;
		
		double[][] toFind = {
				{25.89, 47.56},
				{24, 45.15},
				{25.33, 45.44}
		};
		
		try {
			stringLearningSet = FileUtils.readLearningSetFromFile("data.csv");
			learningSet = getDoubleLearningSet(stringLearningSet);
			classes = getClassesArray(stringLearningSet);
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patters and %s features", numberOfPatterns, numberOfFeatures));
			
			for(int i=0; i<toFind.length;i++) {
				System.out.println(toFind[i][0] + " " + toFind[i][1]);
				PriorityQueue<Place> pq = new PriorityQueue<Place>(31, Collections.reverseOrder());
				for(int j=0; j<learningSet.length; j++) {
					double distance = DistanceUtils.euclidianDistanceGeneralized(toFind[i], learningSet[j]);
//					System.out.println("DIST: " + distance + " " + pq.size());
					if(pq.size() <31) {
//						System.out.println("Mai mic 31");
						pq.add(new Place(stringLearningSet[j], distance));
					} else {
//						System.out.println("Else");
						if(distance < ((Place)pq.peek()).distance) {
//							System.out.println("Mai mare");
							pq.poll();
							pq.add(new Place(stringLearningSet[j], distance));
						}
					}
				}
				
				List<Place> nearest = new ArrayList<>(31);
				while(pq.size()!=0) {
					Place p = (Place)pq.poll();
					nearest.add(p);
				}
				nearest.sort(Comparator.naturalOrder());
				writeClass(9,nearest);
				writeClass(11,nearest);
				writeClass(17,nearest);
				writeClass(31,nearest);

//				System.out.print("9 neighbours: ");
//				HashMap<String, Integer> nearest9 = new HashMap<>();
//				for(int j=0; j<9;j++) {
//					Place peek = nearest.get(j);
//					String name = peek.pattern[peek.pattern.length -1];
//					if(!nearest9.containsKey(name)) {
//						nearest9.put(name, 0);
//					}
//					nearest9.put(name, nearest9.get(name) +1);
//				}
//				
//				System.out.println(Collections.max(nearest9.entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getKey());
			}
			
			
			
//			distanceMatrix = new double[numberOfPatterns][];
//			for(int i=0; i< learningSet.length;i++) {
//				distanceMatrix[i] = new double[numberOfPatterns];				
//			}
//			
//			for(int i=0; i< learningSet.length;i++) {
//				for(int j=i+1; j< learningSet.length;j++) {
//					distanceMatrix[i][j] = distanceMatrix[j][i]= DistanceUtils.euclidianDistanceGeneralized(learningSet[i], learningSet[j]);
//				}
//			}
//			
//			FileUtils.writeLearningSetToFile("distanceMatrix.out", distanceMatrix);
//			
//			for(int i=0; i<stringLearningSet.length;i++) {
//				if(stringLearningSet[i].length < FileUtils.LENGTH_WITH_CLASS) {
//					int pos = 0;
//					double minValue = distanceMatrix[i][0];
//					for(int j=1; j<distanceMatrix[i].length;j++) {
//						if(distanceMatrix[i][j]!=0) {
//							if(distanceMatrix[i][j] < minValue) {
//								minValue = distanceMatrix[i][j];
//								pos = j;								
//							}
//						}
//					}
//
//					System.out.println("Nearest Neighbour distance: " + minValue);
//					System.out.println(i + "th Form is in " + pos + "'s form class -> " + classes[pos]);
//				}
//			}
//			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
		
	}
	
	private static void writeClass(int neighboursNumber, List<Place> nearest) {
		System.out.print(neighboursNumber + " neighbours: ");
		HashMap<String, Integer> nearestMap = new HashMap<>();
		for(int j=0; j<neighboursNumber;j++) {
			Place peek = nearest.get(j);
			String name = peek.pattern[peek.pattern.length -1];
			if(!nearestMap.containsKey(name)) {
				nearestMap.put(name, 0);
			}
			nearestMap.put(name, nearestMap.get(name) +1);
		}
		
		System.out.println(Collections.max(nearestMap.entrySet(), (entry1, entry2) -> entry1.getValue() - entry2.getValue()).getKey());
	}
	
	private static double[][] getDoubleLearningSet(String[][] learningSet){
		double[][] doubleLearningSet = new double[learningSet.length][];
		for(int i=0; i< learningSet.length;i++) {
			doubleLearningSet[i] = new double[learningSet[i].length];
			for(int j=0; j<learningSet[i].length; j++) {
				try {
					doubleLearningSet[i][j] = Double.valueOf(learningSet[i][j]);
					//System.out.print(doubleLearningSet[i][j] + " ");
					
				} catch(NumberFormatException| ArrayIndexOutOfBoundsException e ) {
					
				}
			}
			System.out.println();
		}
		
		return doubleLearningSet;
	}
	
	private static String[] getClassesArray(String[][] learningSet) {
		String[] array = new String[learningSet.length];
		for(int i=0; i<learningSet.length;i++) {
			array[i] =learningSet[i][learningSet[i].length -1]; 
			//System.out.println(array[i]);
		}
		return array;
	}
}
